package murraco;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class SortingAlgorithmsTest {

    // BubbleSort
    @Test
    public void bubbleSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        BubbleSort.bubbleSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    @Test
    public void bubbleSort_random_input_including_negative_integer_values() {
        final Integer[] data = {4, 3, 0, 11, -25, 7, 5, 15, 12, -150, 99, 1};
        BubbleSort.bubbleSort(data);
        assertEquals("[-150, -25, 0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    @Test
    public void bubbleSort_random_input_including_duplicate_integer_values() {
        final Integer[] data = {4, 3, 0, 11, -25, -25, 7, 5, 15, 12, -150, 99, 99, 1};
        BubbleSort.bubbleSort(data);
        assertEquals("[-150, -25, -25, 0, 1, 3, 4, 5, 7, 11, 12, 15, 99, 99]", Arrays.toString(data));
    }

    // HeapSort
    @Test
    public void heapSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        Heapsort.heapSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    @Test
    public void heapSort_strictly_increasing_integer_values() {
        final Integer[] data = {0, 1, 3, 4, 5, 7, 11, 12, 15, 99};
        Heapsort.heapSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    // InsertionSort
    @Test
    public void insertionSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        InsertionSort.insertionSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    // MergeSort
    @Test
    public void mergeSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        MergeSort.mergeSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    @Test
    public void mergeSort_strictly_decreasing_integer_values() {
        final Integer[] data = {99, 15, 12, 11, 7, 5, 4, 3, 1, 0};
        MergeSort.mergeSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    // QuickSort
    @Test
    public void quickSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        Quicksort.quickSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }

    // SelectionSort
    @Test
    public void selectionSort() {
        final Integer[] data = {4, 3, 0, 11, 7, 5, 15, 12, 99, 1};
        SelectionSort.selectionSort(data);
        assertEquals("[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]", Arrays.toString(data));
    }
}
